﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace ConsoleClient
{
    class Program
    {
        static async Task Main()
        {
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            var tokenResonse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest()
            {
                Address = disco.TokenEndpoint,

                ClientId = "console.client",
                ClientSecret = "secret",
                Scope = "weather_api"
            });

            if (tokenResonse.IsError)
            {
                Console.WriteLine(tokenResonse.Error);
                return;
            }

            Console.WriteLine(tokenResonse.Json);

            var apiCLient = new HttpClient();
            apiCLient.SetBearerToken(tokenResonse.AccessToken);

            var response = await apiCLient.GetAsync("https://localhost:6001/weatherforecast");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                using var doc = JsonDocument.Parse(content);
                var text = JsonSerializer.Serialize(doc.RootElement.Clone(),
                    new JsonSerializerOptions(JsonSerializerDefaults.Web) {WriteIndented = true});
                Console.WriteLine(text);
            }
        }
    }
}